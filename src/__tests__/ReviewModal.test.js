import {configure, mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import React from "react";
import 'jsdom-global/register';
import { MockedProvider } from '@apollo/react-testing';
import ReviewModal from "../components/Modals/ReviewModal/ReviewModal";

configure({ adapter: new Adapter() });

describe('review modal', () => {
  const wrapper = mount(<MockedProvider>
    <ReviewModal
      open={true}
      page={1}
      rowsPerPage={5}
      selectedProposal={{}}
      handleClose={() => {}}
    />
  </MockedProvider>);
  it('should match snapshot ReviewModal', () => {
    expect(wrapper).toMatchSnapshot();
  })
});