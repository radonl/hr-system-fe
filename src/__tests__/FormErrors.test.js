import {configure, mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import React from "react";
import 'jsdom-global/register';
import {FormErrors} from "../components/Forms/FormErrors/FormErrors";

configure({ adapter: new Adapter() });

describe('test Dropdown', () => {
  const form = {
    test: {
      valid: false,
      value: 'test'
    }
  };
  const wrapper = mount(<FormErrors form={form} mt={1}/>);
  it('should match snapshot FormErrors', () => {
    expect(wrapper).toMatchSnapshot();
  });
  it('should display error', () => {
    const p = wrapper.find('p').getDOMNode();
    expect(p.textContent).toBe('*Test is incorrect!')
  })
});