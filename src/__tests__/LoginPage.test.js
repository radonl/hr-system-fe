import {configure, mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { MockedProvider } from '@apollo/react-testing';
import React from "react";
import 'jsdom-global/register';
import LoginPage from "../pages/LoginPage/LoginPage";


configure({ adapter: new Adapter() });



describe('login page', () => {
  const loginPage = <LoginPage/>;
  const wrapper = mount(<MockedProvider>
    {loginPage}
  </MockedProvider>);
  it('match snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });

});