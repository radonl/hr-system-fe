import {configure, mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import React from "react";
import 'jsdom-global/register';
import FormActionButton from "../components/Forms/Buttons/FormActionButton/FormActionButton";

configure({ adapter: new Adapter() });

describe('test Dropdown', () => {
  const wrapper = mount(<FormActionButton>test</FormActionButton>);
  it('should match snapshot FormActionButton', () => {
    expect(wrapper).toMatchSnapshot();
  });
  it('should have text', () => {
    const button = wrapper.find('button').getDOMNode();
    expect(button.textContent).toBe('test');
  })
});
