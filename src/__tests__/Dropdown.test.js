import {configure, mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import React from "react";
import 'jsdom-global/register';
import Dropdown from "../components/Forms/DropDowns/Dropdown/Dropdown";

configure({ adapter: new Adapter() });

describe('test Dropdown', () => {
  const items = [{
    _id: 'test',
    name: 'test'
  }];
  const wrapper = mount(<Dropdown items={items} entityName={'test'}/>);
  it('should match snapshot Dropdown', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('should exist items', () => {
    const firstOption = wrapper.find('option').first().getDOMNode();
    const lastOption = wrapper.find('option').last().getDOMNode();
    expect(firstOption.text).toBe('Select test');
    expect(lastOption.text).toBe('test');
  })
});
