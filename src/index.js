import ReactDOM from 'react-dom';
import './tailwind.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import {createHttpLink} from 'apollo-link-http';
import { ApolloLink } from 'apollo-link';
import { ApolloProvider } from 'react-apollo';
import React from "react";
import './styles.css';

const httpLink = createHttpLink({uri: process.env.REACT_APP_API_URL});

const JWT = localStorage.getItem('token');

const middlewareLink = new ApolloLink((operation, forward) => {
  operation.setContext({
    headers: {
      ...JWT ? {JWT} : {}
    }
  });
  return forward(operation).map(result => {
    const context = operation.getContext();
    if(context && context.headers && context.headers.JWT) {
      localStorage.setItem('token', context.headers.JWT);
    }
    if(context.response.status === 401) {
      localStorage.removeItem('token');
      window.location.reload();
    }
    return result;
  })
});

const link = middlewareLink.concat(httpLink);

const client = new ApolloClient({
  link,
  cache: new InMemoryCache()
});

ReactDOM.render(
  <ApolloProvider client={client}>
    <App/>
  </ApolloProvider>
    , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
