import React from 'react';
import {Redirect, Route, Router, Switch} from "react-router-dom";
import LoginPage from "./pages/LoginPage/LoginPage";
import {history} from "./helpers/history";
import Dashboard from "./pages/Proposals/Proposals";
import AuthorizedLayout from "./components/Layouts/AuthorizedLayout/AuthorizedLayout";

const token = localStorage.getItem('token');

const App = () => {
  return <Router history={history}>
    <Route render={({location}) => (
      <div className="App">
        <Switch location={location}>
          {token ? <Route path='/dashboard' component={AuthorizedLayout}/> : <Route path='/login' component={LoginPage}/>}
          {token ? <Redirect from="*" to="/dashboard"/> : <Redirect from="*" to="/login"/>}
        </Switch>
      </div>
    )}/>
  </Router>
};

export default App;
