import React, {useState} from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import {useQuery} from "@apollo/react-hooks";
import TablePagination from "@material-ui/core/TablePagination";
import RateReviewIcon from '@material-ui/icons/RateReview';

import {GET_ME, GET_PROPOSALS} from "../../queries";
import FormActionButton from "../../components/Forms/Buttons/FormActionButton/FormActionButton";
import ProposalModal from "../../components/Modals/ProposalModal/ProposalModal";
import ReviewModal from "../../components/Modals/ReviewModal/ReviewModal";
import Spinner from "../../components/Forms/Spinner/Spinner";
import RemoveProposalModal from "../../components/Modals/RemoveProposalModal/RemoveProposalModal";
import TextField from "@material-ui/core/TextField";
import TableSortLabel from "@material-ui/core/TableSortLabel";

export default () => {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [proposalIsOpen, setProposalIsOpen] = useState(false);
  const [removeProposalIsOpen, setRemoveProposalIsOpen] = useState(false);
  const [reviewIsOpen, setReviewIsOpen] = useState(false);
  const [selectedProposal, setSelectedProposal] = useState({});
  const [searchString, setSearchString] = useState('');
  const [orderBy, setOrderBy] = useState('email');
  const [order, setOrder] = useState('asc');
  const fullAccessRoles = ['Administrator', 'HR'];
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };
  const handleChangeRowsPerPage = event => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
  const handleSortChanged = (field) => {
    if(field === orderBy) {
      setOrder(order === 'asc' ? 'desc' : 'asc');
    } else {
      setOrder('asc');
      setOrderBy(field);
    }
  };
  const variables = {
    skip: page * rowsPerPage,
    limit: rowsPerPage,
    order,
    orderBy,
    searchString
  };
  const {loading, data} = useQuery(GET_PROPOSALS, {
    variables
  });
  const me = useQuery(GET_ME).data;
  return (
    <div className="p-3">
      <div className="bg-gray-100 justify-end flex justify-between p-2">
        <TextField label="Search" value={searchString} type="search" onChange={e => setSearchString(e.target.value)}/>
        <FormActionButton width={64} onClick={() => {
          setProposalIsOpen(true);
          setSelectedProposal({});
        }}>Create new proposal</FormActionButton>
      </div>
      <TableContainer component={Paper}>
        <Table aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Avatar</TableCell>
              <TableCell
                align='left'
                sortDirection={orderBy === 'candidate.email' ? order : false}
              >
                <TableSortLabel
                  active={orderBy === 'candidate.email'}
                  direction={orderBy === 'candidate.email' ? order : 'asc'}
                  onClick={() => handleSortChanged('candidate.email')}
                >
                  E-mail
                </TableSortLabel>
              </TableCell>
              <TableCell
                align='left'
                sortDirection={orderBy === 'candidate.firstName' ? order : false}
              >
                <TableSortLabel
                  active={orderBy === 'candidate.firstName'}
                  direction={orderBy === 'candidate.firstName' ? order : 'asc'}
                  onClick={() => handleSortChanged('candidate.firstName')}
                >
                  First name
                </TableSortLabel>
              </TableCell>
              <TableCell
                align='left'
                sortDirection={orderBy === 'candidate.lastName' ? order : false}
              >
                <TableSortLabel
                  active={orderBy === 'candidate.lastName'}
                  direction={orderBy === 'candidate.lastName' ? order : 'asc'}
                  onClick={() => handleSortChanged('candidate.lastName')}
                >
                  Last Name
                </TableSortLabel>
              </TableCell>
              <TableCell
                align='left'
                sortDirection={orderBy === 'status' ? order : false}
              >
                <TableSortLabel
                  active={orderBy === 'status'}
                  direction={orderBy === 'status' ? order : 'asc'}
                  onClick={() => handleSortChanged('status')}
                >
                  Status
                </TableSortLabel>
              </TableCell>
              <TableCell
                align='left'
                sortDirection={orderBy === 'scheduledInterviewTime' ? order : false}
              >
                <TableSortLabel
                  active={orderBy === 'scheduledInterviewTime'}
                  direction={orderBy === 'scheduledInterviewTime' ? order : 'asc'}
                  onClick={() => handleSortChanged('scheduledInterviewTime')}
                >
                  Scheduled interview time
                </TableSortLabel>
              </TableCell>
              <TableCell align="left">Reviews</TableCell>
              {me.me && me.me.role && fullAccessRoles.includes(me.me.role.name) && <TableCell align="left"/>}
              {me.me && me.me.role && fullAccessRoles.includes(me.me.role.name) && <TableCell align="left"/>}
            </TableRow>
          </TableHead>
          <TableBody>
            {(loading ? [] : data.proposals).map(row => (
              <TableRow key={row._id}>
                <TableCell component="th" scope="row">
                  <div className="rounded-full w-10 h-10 bg-gray-300 bg-cover"
                       style={{backgroundImage: row.candidate.avatarFilePath ? `url(${row.candidate.avatarFilePath})` : ''}}
                  />
                </TableCell>
                <TableCell align="left">{row.candidate.email}</TableCell>
                <TableCell align="left">{row.candidate.firstName}</TableCell>
                <TableCell align="left">{row.candidate.lastName}</TableCell>
                <TableCell align="left">{row.status}</TableCell>
                <TableCell align="left">{row.scheduledInterviewTime}</TableCell>
                <TableCell align="left">
                  <FormActionButton color='blue' onClick={() => {
                    setReviewIsOpen(true);
                    setSelectedProposal(row)
                  }}>
                    <div className={'w-11 flex justify-between items-center'}>
                      <RateReviewIcon/>
                      <span>{row.reviews.length}</span>
                    </div>
                  </FormActionButton>
                </TableCell>
                {me.me && me.me.role && fullAccessRoles.includes(me.me.role.name) &&
                  <TableCell align="left">
                    <FormActionButton color='yellow' onClick={() => {
                      setSelectedProposal(row);
                      setProposalIsOpen(true);
                    }}>
                      Edit
                    </FormActionButton>
                  </TableCell>
                }
                {me.me && me.me.role && fullAccessRoles.includes(me.me.role.name) &&
                  <TableCell align="left">
                    <FormActionButton color='red' onClick={() => {
                      setRemoveProposalIsOpen(true);
                      setSelectedProposal(row)
                    }}>
                      Remove
                    </FormActionButton>
                  </TableCell>
                }
              </TableRow>
            ))}
            {loading && <TableRow>
              <TableCell colSpan={9} className={'h-365'}>
                <Spinner />
              </TableCell>
            </TableRow>}
          </TableBody>
        </Table>
      </TableContainer>
      {!loading && <TablePagination
        rowsPerPageOptions={[5, 10, 25, 100]}
        component="div"
        count={data.proposalsCount}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />}
      <ProposalModal open={proposalIsOpen}
                     handleClose={() => {
                       setProposalIsOpen(false);
                       setSelectedProposal({});
                     }}
                     variables={variables}
                     selectedProposal={selectedProposal}
      />
      <RemoveProposalModal open={removeProposalIsOpen}
                         handleClose={() => setRemoveProposalIsOpen(false)}
                         page={page}
                         rowsPerPage={rowsPerPage}
                         selectedProposal={selectedProposal}
      />
      <ReviewModal open={reviewIsOpen}
                   handleClose={() => setReviewIsOpen(false)}
                   page={page}
                   rowsPerPage={rowsPerPage}
                   selectedProposal={selectedProposal}
      />
    </div>
  );
}