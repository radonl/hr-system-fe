import React, {useState} from "react";
import {useQuery} from "@apollo/react-hooks";
import {GET_USERS} from "../../queries";
import FormActionButton from "../../components/Forms/Buttons/FormActionButton/FormActionButton";
import TableContainer from "@material-ui/core/TableContainer/TableContainer";
import Paper from "@material-ui/core/Paper/Paper";
import Table from "@material-ui/core/Table/Table";
import TableHead from "@material-ui/core/TableHead/TableHead";
import TableRow from "@material-ui/core/TableRow/TableRow";
import TableCell from "@material-ui/core/TableCell/TableCell";
import TableBody from "@material-ui/core/TableBody/TableBody";
import Spinner from "../../components/Forms/Spinner/Spinner";
import TablePagination from "@material-ui/core/TablePagination/TablePagination";
import RemoveUserModal from "../../components/Modals/RemoveUserModal/RemoveUserModal";
import UserModal from "../../components/Modals/UsersModal/UsersModal";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import TextField from "@material-ui/core/TextField";

const Users = () => {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [userIsOpen, setUserIsOpen] = useState(false);
  const [selectedUser, setSelectedUser] = useState({});
  const [orderBy, setOrderBy] = useState('email');
  const [order, setOrder] = useState('asc');
  const [searchString, setSearchString] = useState('');
  const [removeUserIsOpen, setRemoveUserIsOpen] = useState(false);
  const variables = {
    skip: page * rowsPerPage,
    limit: rowsPerPage,
    order,
    orderBy,
    searchString
  };
  const {loading, data} = useQuery(GET_USERS, {
    variables
  });
  const handleSortChanged = (field) => {
    if(field === orderBy) {
      setOrder(order === 'asc' ? 'desc' : 'asc');
    } else {
      setOrder('asc');
      setOrderBy(field);
    }
  };
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
  return <div className="p-3">
    <div className="bg-gray-100 justify-end flex justify-between p-2">
      <TextField label="Search" value={searchString} type="search" onChange={e => setSearchString(e.target.value)}/>
      <FormActionButton width={64} onClick={() => {
        setUserIsOpen(true);
        setSelectedUser({});
      }}>Create new user</FormActionButton>
    </div>
    <TableContainer component={Paper}>
      <Table aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Avatar</TableCell>
            <TableCell
              align='left'
              sortDirection={orderBy === 'email' ? order : false}
            >
              <TableSortLabel
                active={orderBy === 'email'}
                direction={orderBy === 'email' ? order : 'asc'}
                onClick={() => handleSortChanged('email')}
              >
                E-mail
              </TableSortLabel>
            </TableCell>
            <TableCell
              align='left'
              sortDirection={orderBy === 'firstName' ? order : false}
            >
              <TableSortLabel
                active={orderBy === 'firstName'}
                direction={orderBy === 'firstName' ? order : 'asc'}
                onClick={() => handleSortChanged('firstName')}
              >
                First name
              </TableSortLabel>
            </TableCell>
            <TableCell
              align='left'
              sortDirection={orderBy === 'lastName' ? order : false}
            >
              <TableSortLabel
                active={orderBy === 'lastName'}
                direction={orderBy === 'lastName' ? order : 'asc'}
                onClick={() => handleSortChanged('lastName')}
              >
                Last name
              </TableSortLabel>
            </TableCell>
            <TableCell align='left'>Role</TableCell>
            <TableCell align="left"/>
            <TableCell align="left"/>
          </TableRow>
        </TableHead>
        <TableBody>
          {(loading ? [] : data.users).map(row => (
            <TableRow key={row._id}>
              <TableCell component="th" scope="row">
                <div className="rounded-full w-10 h-10 bg-gray-300 bg-cover"
                     style={{backgroundImage: row.avatarFilePath ? `url(${row.avatarFilePath})` : ''}}
                />
              </TableCell>
              <TableCell align="left">{row.email}</TableCell>
              <TableCell align="left">{row.firstName}</TableCell>
              <TableCell align="left">{row.lastName}</TableCell>
              <TableCell align="left">{row.role.name}</TableCell>

              <TableCell align="left">
                <FormActionButton color='yellow' onClick={() => {
                  setSelectedUser(row);
                  setUserIsOpen(true);
                }}>
                  Edit
                </FormActionButton>
              </TableCell>

              <TableCell align="left">
                <FormActionButton color='red' onClick={() => {
                  setRemoveUserIsOpen(true);
                  setSelectedUser(row)
                }}>
                  Remove
                </FormActionButton>
              </TableCell>
            </TableRow>
          ))}
          {loading && <TableRow>
            <TableCell colSpan={9} className={'h-365'}>
              <Spinner />
            </TableCell>
          </TableRow>}
        </TableBody>
      </Table>
    </TableContainer>
    {!loading && <TablePagination
      rowsPerPageOptions={[5, 10, 25, 100]}
      component="div"
      count={data.usersCount}
      rowsPerPage={rowsPerPage}
      page={page}
      onChangePage={handleChangePage}
      onChangeRowsPerPage={handleChangeRowsPerPage}
    />}
    <RemoveUserModal open={removeUserIsOpen}
                     handleClose={() => setRemoveUserIsOpen(false)}
                     variables={variables}
                     selectedUser={selectedUser}
    />
    <UserModal open={userIsOpen}
               handleClose={() => {
                 setUserIsOpen(false);
                 setSelectedUser({})
               }}
               selectedUser={selectedUser}
               variables={variables}
    />
  </div>
};

export default Users;