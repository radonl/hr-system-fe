import React from 'react';
import {useMutation} from '@apollo/react-hooks';

import {FormErrors} from '../../components/Forms/FormErrors/FormErrors';
import mainStateHook from '../../helpers/mainStateHook';
import FormInput from '../../components/Forms/FormInput/FormInput';
import FormActionButton from '../../components/Forms/Buttons/FormActionButton/FormActionButton';
import Spinner from '../../components/Forms/Spinner/Spinner';
import {AUTH_USER} from '../../mutations';

const LoginPage = () => {
  const [loginForm, setLoginForm] = mainStateHook({
    email: {
      valid: false,
      value: '',
    },
    password: {
      valid: false,
      value: ''
    },
    requestInProgress: false,
    valid: true
  });
  const [auth, {credentials}] = useMutation(AUTH_USER);
  const formIsValid = () => loginForm.email.valid && loginForm.password.valid;
  const validator = (e, fieldName) => {
    const value = e.target.value;
    switch (fieldName) {
      case 'email':
        loginForm.email.valid = !!value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
        loginForm.email.value = value;
        break;
      case 'password':
        loginForm.password.valid = !!value.match(/^.*(?=.{8,})((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/);
        loginForm.password.value = value;
        break;
      default:
        break;
    }
    loginForm.valid = formIsValid();
    setLoginForm(loginForm);
  };
  const sendAuthRequest = () => {
    setLoginForm({requestInProgress: true});
    auth({ variables: { email: loginForm.email.value, password: loginForm.password.value } }).then(res => {
      if(res.data.login === 'false') {
        loginForm.password.valid = false;
        loginForm.valid = false;
        loginForm.requestInProgress = false;
        setLoginForm(loginForm);
      } else {
        localStorage.setItem('token', res.data.login);
        window.location.href = '/dashboard';
      }
    });
  };

  return (
    <div className="w-full mt-20 flex justify-center">
      <div className="w-full max-w-xs">
        <form className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
          <div className="mb-4">
            <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="email">
              E-mail
            </label>
            <FormInput
              value={loginForm.email.value}
              onChange={(e) => validator(e, 'email')}
              isValid={loginForm.email.valid || !loginForm.email.value}
              id="email"
              type="email"
              placeholder="E-mail"
            />
          </div>
          <div className="mb-6">
            <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="password">
              Password
            </label>
            <FormInput
              value={loginForm.password.value}
              onChange={(e) => validator(e, 'password')}
              isValid={loginForm.password.valid || !loginForm.password.value}
              id="password"
              type="password"
              placeholder="******************"
            />
            {!loginForm.valid && <FormErrors mt={5} form={loginForm} />}
          </div>
          <div className="flex items-center justify-center">
            {
              loginForm.requestInProgress ?
                <Spinner /> :
                <FormActionButton type="button" onClick={() => sendAuthRequest()}>
                  Sign In
                </FormActionButton>
            }
          </div>
        </form>
      </div>
    </div>
  );
};

export default LoginPage;
