const PROPOSAL_STATUSES = [{
  id: 1, name: 'Waiting for an interview'
}, {
  id: 2, name: 'Successful interview'
}, {
  id: 3, name: 'Failed interview'
}];

export {PROPOSAL_STATUSES};