import gql from 'graphql-tag';

const CREATE_SKILL = gql`
  mutation ($name:String!) {
    createSkill(name:$name) {
      _id
      name
    }
  } 
`;

export {CREATE_SKILL};
