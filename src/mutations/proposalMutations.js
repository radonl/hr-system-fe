import gql from 'graphql-tag';

const CREATE_PROPOSAL = gql`
  mutation(
    $firstName: String!
    $lastName: String!
    $middleName: String
    $email: String!
    $avatar:FileInput
    $cv:FileInput
    $desiredSalary:Float
    $suggestedSalary:Float
    $scheduledInterviewTime:String
    $skills:[String]
    $techEmployees:[String]
    $phone: String!
  ) {
    createProposal(
      firstName: $firstName
      lastName: $lastName
      middleName: $middleName
      email: $email,
      desiredSalary: $desiredSalary
      suggestedSalary: $suggestedSalary
      scheduledInterviewTime: $scheduledInterviewTime
      skills: $skills
      techEmployees: $techEmployees
      cv: $cv
      avatar: $avatar
      phone: $phone
    ) {
      _id
    }
  }
`;

const UPDATE_PROPOSAL = gql`
  mutation(
    $firstName: String!
    $lastName: String!
    $middleName: String
    $email: String!
    $avatar:FileInput
    $cv:FileInput
    $desiredSalary:Float
    $suggestedSalary:Float
    $scheduledInterviewTime:String
    $skills:[String]
    $techEmployees:[String]
    $_id:String!
    $status: String!
    $phone: String!
  ) {
    updateProposal(
      firstName: $firstName
      lastName: $lastName
      middleName: $middleName
      email: $email,
      desiredSalary: $desiredSalary
      suggestedSalary: $suggestedSalary
      scheduledInterviewTime: $scheduledInterviewTime
      skills: $skills
      techEmployees: $techEmployees
      cv: $cv
      avatar: $avatar
      _id: $_id
      status: $status
      phone: $phone
    ) {
      _id
    }
  }
`;

const REMOVE_PROPOSAL = gql`
  mutation($id:String!){
    removeProposal(id:$id){
      _id
    }
  }
`;

const CREATE_REVIEW = gql`
mutation ($_id:String!, $message:String!) {
  createReview(_id: $_id, message: $message) {
    _id
  }
}
`;

export {CREATE_PROPOSAL, UPDATE_PROPOSAL, REMOVE_PROPOSAL, CREATE_REVIEW};