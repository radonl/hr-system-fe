import gql from 'graphql-tag';

const AUTH_USER = gql`
  mutation ($email:String!, $password:String!) {
    login (email:$email, password:$password)
  }
`;

const LOGOUT = gql`
  mutation {
    logout
  }
`;

const CREATE_USER = gql`
  mutation(
    $email: String!
    $password: String!
    $firstName: String!
    $lastName: String!
    $roleId:String!
    $avatar:FileInput
    $middleName:String
    ) {
      createUser(
        firstName: $firstName
        email: $email
        password: $password
        lastName: $lastName
        roleId:$roleId
        middleName:$middleName
        avatar: $avatar
      ) {
        _id
      }
    }
`;

const UPDATE_USER = gql`
  mutation(
    $email: String!
    $password: String!
    $firstName: String!
    $lastName: String!
    $roleId:String!
    $avatar:FileInput
    $middleName:String,
    $_id:String!
  ) {
    updateUser(
      _id: $_id
      firstName: $firstName
      email: $email
      password: $password
      lastName: $lastName
      roleId:$roleId
      middleName:$middleName
      avatar: $avatar
    ) {
      _id
    }
  }
`;

const REMOVE_USER = gql`
  mutation(
    $_id:String!
  ) {
    removeUser(_id:$_id){
      _id
    }
  }
`;

export {AUTH_USER, LOGOUT, CREATE_USER, UPDATE_USER, REMOVE_USER};
