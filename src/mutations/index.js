import {AUTH_USER, CREATE_USER, UPDATE_USER, REMOVE_USER, LOGOUT} from './userMutations';
import {CREATE_SKILL} from "./skillsMutations";
import {CREATE_PROPOSAL, REMOVE_PROPOSAL, UPDATE_PROPOSAL, CREATE_REVIEW} from './proposalMutations';

export {
  AUTH_USER,
  CREATE_USER,
  UPDATE_USER,
  REMOVE_USER,
  CREATE_SKILL,
  CREATE_PROPOSAL,
  UPDATE_PROPOSAL,
  REMOVE_PROPOSAL,
  LOGOUT,
  CREATE_REVIEW
};
