import gql from 'graphql-tag';

const GET_SKILLS = gql`
  {
    skills {
      _id,
       name
    }
  }
`;

export {GET_SKILLS};
