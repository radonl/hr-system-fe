import gql from 'graphql-tag';

const GET_PROPOSALS = gql`
query($skip: Int!, $limit: Int!, $searchString:String, $order:String, $orderBy:String) {
  proposals(skip: $skip, limit: $limit, searchString: $searchString, order:$order, orderBy:$orderBy) {
    _id
    desiredSalary
    suggestedSalary
    scheduledInterviewTime
    status
    cv {
      fileName
      fileContent
    }
    reviews {
      _id
      reviewer{
        firstName
        lastName
        middleName
        email
        avatarFilePath
        role {
          name
        }
      }
      message
    }
    candidate {
      firstName
      lastName
      middleName
      email
      phone
      avatarFilePath
    }
    techEmployees {
      _id
      firstName
      lastName
      middleName
      email
      name
      role {
        name
      }
    }
    initiator {
      _id
      firstName
      lastName
      middleName
      email
      name
      role{
        name
      }
    }
    skills {
      _id
      name
    }
  }
  proposalsCount(searchString: $searchString)
}
`;

export {GET_PROPOSALS};