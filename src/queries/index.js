import {GET_USER, GET_USERS, GET_ME} from './userQueries';
import {GET_SKILLS} from './skillsQueries';
import {GET_PROPOSALS} from './proposalQueries';
import {GET_ROLES} from "./roleQueries";

export {GET_USER, GET_USERS, GET_ME, GET_SKILLS, GET_PROPOSALS, GET_ROLES};
