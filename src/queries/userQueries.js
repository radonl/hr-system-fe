import gql from 'graphql-tag';

const GET_USERS = gql`
   query(
    $onlyTechEmployees: Boolean
    $onlyHRs: Boolean
    $skip: Int!
    $limit: Int!
    $order: String
    $orderBy:String
    $searchString:String
  ) {
    users(
      onlyTechEmployees: $onlyTechEmployees
      onlyHRs: $onlyHRs
      skip: $skip
      limit: $limit
      order:$order
      orderBy:$orderBy
      searchString:$searchString
    ) {
      _id
      firstName
      lastName
      middleName
      email
      avatarFilePath
      name
      role {
        _id
        name
      }
    }
    usersCount(onlyTechEmployees: $onlyTechEmployees, onlyHRs: $onlyHRs, searchString: $searchString)
  }
`;

const GET_USER = gql`
  query ($id:String){
    user (_id:$id) {
      _id
      firstName
      lastName
      middleName
      email
      role {
        name
      }
    }
  }
`;

const GET_ME = gql`
  {
    me {
      _id
      firstName
      lastName
      middleName
      email
      avatarFilePath
      role {
        name
      }
    }
  }
`;

export {GET_USER, GET_USERS, GET_ME};
