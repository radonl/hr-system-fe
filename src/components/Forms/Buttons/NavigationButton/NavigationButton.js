import React from "react";

const NavigationButton = ({ children, ...props }) => {
  return <button
    className="h-full bg-white hover:bg-gray-300 text-black font-bold px-10 focus:outline-none focus:shadow-outline"
    {...props}>
    {children}
  </button>
};

export default NavigationButton;
