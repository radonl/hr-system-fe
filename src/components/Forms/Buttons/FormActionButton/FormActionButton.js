import React from "react";

const FormActionButton = ({ children, ...props, width, color, disabled}) => {
  return <button
    className={`
       w-${width ? width : 'full'} bg-${color ? color : 'blue'}-500
       text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline
       ${disabled ? 'opacity-50 cursor-not-allowed' : `hover:bg-${color ? color : 'blue'}-700`}
     `}
    {...props}>
    {children}
  </button>
};

export default FormActionButton;
