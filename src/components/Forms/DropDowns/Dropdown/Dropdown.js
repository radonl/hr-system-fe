import React from "react";

const Dropdown = ({items, width, isValid, entityName, ...props}) => {
  return <select
    className={`
      border ${isValid?'':'border-red-500'} 
      rounded w-${width ? width : 'full'} py-2 px-3 text-gray-700 
      focus:outline-none focus:shadow-outline bg-white shadow
    `}
    {...props}
  >
    <option value='' disabled>Select {entityName}</option>
    {items.map((item, index) => (
      <option value={item._id} key={`${item._id}.${index}`}>{item.name}</option>
    ))}
  </select>
};

export default Dropdown
