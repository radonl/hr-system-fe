import React, {useState} from "react";
import Chip from "@material-ui/core/Chip";
import Autocomplete from "@material-ui/lab/Autocomplete";
import TextField from "@material-ui/core/TextField";

const MultiSelect = ({items, selectedItems, loading, addHandler, removeHandler, label}) => {
  const [newValue, setNewValue] = useState('');
  const addHandlerInner = (e) => {
    if (e.key === 'Enter') {
      addHandler(e.target.value);
      setNewValue('');
    }
  };
  return <div className={"bg-gray-200 flex mt-2 p-5 flex-wrap"}>
    <div>
      {selectedItems.map((item, index) => {
        return (
          <Chip
            className={"m-1"}
            key={`${item._id}.${index}`}
            label={item.name}
            onDelete={() => removeHandler(item)}
          />
        );
      })}
    </div>
    <div className="p-2 w-full">
      <Autocomplete
        freeSolo
        loading={loading}
        value={newValue ? newValue : ''}
        onKeyPress={e => addHandlerInner(e)}
        onChange={e => setNewValue(e.target.value)}
        options={(loading ? [] : items).map(option => option.name)}
        renderInput={params => (
          <TextField
            {...params}
            label={label}
            InputProps={{...params.InputProps, type: 'search'}}
          />
        )}
      />
    </div>
  </div>;
};

export default MultiSelect;