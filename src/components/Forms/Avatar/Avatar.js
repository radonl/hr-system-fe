import React, {useState} from "react";

const Avatar = ({initAvatar, avatarChanged}) => {
  const [avatar, setAvatar] = useState(initAvatar);
  const changeAvatar = (e) => {
    let file = e.target.files[0];
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      const avatarData = {
        fileContent: reader.result,
        fileExtension: file.name.split('.').pop()
      };
      setAvatar(avatarData);
      avatarChanged({
        fileExtension: avatarData.fileExtension,
        fileContent: avatarData.fileContent.split(',')[1]
      });
    };
    reader.onerror = () => {
      console.error(reader.error);
    };
  };
  return <div className={"flex items-center p-5"}>
    <label htmlFor="avatar" className={`rounded-full w-32 h-32 bg-gray-300 bg-cover`}
           style={{backgroundImage: avatar ? `url(${avatar.fileContent})` : ''}}/>
    <input type="file" id='avatar' onChange={e => changeAvatar(e)}/>
  </div>
};

export default Avatar;
