import React, {useState} from "react";
import Chip from "@material-ui/core/Chip";
import {GetApp} from "@material-ui/icons";

const AttachFile = ({label, fileChanged, initFile}) => {
  const [file, setFile] = useState(initFile ? {
    fileContent: initFile.fileContent,
    fileExtension: initFile.fileName.split('.').pop(),
    displayedName: initFile.fileName.length > 24 ? `${initFile.fileName.substring(0, 21)}...` : initFile.fileName,
    originalFileName: initFile.fileName
  } : null);
  const addFile = (e) => {
    let file = e.target.files[0];
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      const fileData = {
        fileContent: reader.result.split(',')[1],
        fileExtension: file.name.split('.').pop(),
      };
      setFile({
        ...fileData,
        originalFileName: file.name,
        displayedName: file.name.length > 24 ? `${file.name.substring(0, 21)}...` : file.name
      });
      fileChanged(fileData);
    };
    reader.onerror = () => {
      console.error(reader.error);
    };
  };

  const base64ToBlob = (base64, type = 'application/octet-stream') => {
    return fetch(`data:${type};base64,${base64}`).then(res => res.blob())
  };

  const download = async () => {
    const a = document.createElement("a");
    document.body.appendChild(a);
    a.style = 'display: none';
    const url = window.URL.createObjectURL(await base64ToBlob(file.fileContent));
    a.href = url;
    a.download = file.originalFileName;
    a.click();
    window.URL.revokeObjectURL(url);
  };

  return <div>
    {file ? <Chip label={file.displayedName}
                  icon={<GetApp />}
                  onDelete={() => setFile(null)}
                  onClick={() => download()}
    /> : <div>
      <label htmlFor="file"
             className={"bg-green-500 focus:outline-none focus:shadow-outline hover:bg-green-600 text-gray-100 p-3 rounded cursor-pointer"}>{label}</label>
      <input id="file" type="file" onChange={e => addFile(e)}/>
    </div>}
  </div>
};

export default AttachFile;
