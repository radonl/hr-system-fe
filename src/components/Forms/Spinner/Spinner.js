import React from "react";

import './Spinner.css';

const Spinner = ({width, height, color, colorWeight}) => {
  return <div
    className={
      `spinner w-${width ? width : 50} h-${height ? height : 50} 
      text-${color ? color : 'green'}-${colorWeight ? colorWeight : 400}
      flex justify-center
      p-5
      `}>
  </div>;
};

export default Spinner;
