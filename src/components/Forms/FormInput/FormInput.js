import React from "react";

const FormInput = ({isValid, ...props, width, margin}) => {
  return <input
    className={`
    shadow appearance-none border ${isValid?'':'border-red-500'} 
    rounded w-${width ? width : 'full'} py-2 px-3 text-gray-700 
    m-${margin?margin:0}  leading-tight focus:outline-none focus:shadow-outline`}
    {...props}
  />
};

export default FormInput;
