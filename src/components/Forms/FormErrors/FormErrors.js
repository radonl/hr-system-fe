import React from "react";

export const FormErrors = ({mt, form}) => {
  return <div className={`mt-${mt}`}>
    {Object.keys(form).map((fieldName, i) => {
      if(!form[fieldName].valid && form[fieldName].value){
        return (
          <p className="text-red-500 text-xs italic" key={i}>*{fieldName.charAt(0).toUpperCase() + fieldName.slice(1)} is incorrect!</p>
        )
      }
      return '';
    })}
  </div>;
};
