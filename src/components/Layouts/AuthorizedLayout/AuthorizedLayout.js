import React from "react";
import {useMutation, useQuery} from "@apollo/react-hooks";
import {GET_ME} from "../../../queries";
import Spinner from "../../Forms/Spinner/Spinner";
import NavigationButton from "../../Forms/Buttons/NavigationButton/NavigationButton";
import {Link, Redirect, Route, Switch} from "react-router-dom";
import Proposals from "../../../pages/Proposals/Proposals";
import {LOGOUT} from "../../../mutations";
import Users from "../../../pages/Users/Users";

const AuthorizedLayout = () => {
  const { loading, error, data } = useQuery(GET_ME);
  const [logout] = useMutation(LOGOUT);
  const logoutHandler = () => {
    logout().then(() => {
      localStorage.removeItem('token');
      window.location.reload();
    }).catch(e => console.error(e));
  };
  return <div>
    {loading && !data ? <Spinner /> : (
      <div className="min-h-screen bg-gray-200">
        <div className="h-90 flex justify-between bg-white">
          <div className="flex justify-center align-content-center flex-col items-center pl-5 pr-5 bg-gray-100">
            <div className="rounded-full w-10 h-10 bg-gray-300 bg-cover"
                 style={{backgroundImage: data.me.avatarFilePath ? `url(${data.me.avatarFilePath})` : ''}}
            />
            <div>
              <span>{data.me.firstName} {data.me.lastName} ({data.me.role.name})</span>
            </div>
          </div>
          <div>
            <Link to='/dashboard/proposals'>
              <NavigationButton >Proposals</NavigationButton>
            </Link>
            {data.me.role.name === 'Administrator' && <Link to='/dashboard/users'>
              <NavigationButton>Users</NavigationButton>
            </Link>}
          </div>
          <div>
            <NavigationButton onClick={() => logoutHandler()}>Logout</NavigationButton>
          </div>
        </div>
        <Route render={({location}) => (
          <div className="App">
            <Switch location={location}>
             <Route path='/dashboard/proposals' component={Proposals}/>
              {data.me.role.name === 'Administrator' && <Route path='/dashboard/users' component={Users}/>}
             <Redirect from="/dashboard" to="/dashboard/proposals"/>
            </Switch>
          </div>
        )}/>
      </div>
    )}
  </div>
};

export default AuthorizedLayout;
