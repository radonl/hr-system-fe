import React, {useEffect, useState} from "react";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import {DialogContent} from "@material-ui/core";
import Avatar from "../../Forms/Avatar/Avatar";
import FormInput from "../../Forms/FormInput/FormInput";
import Dropdown from "../../Forms/DropDowns/Dropdown/Dropdown";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import FormActionButton from "../../Forms/Buttons/FormActionButton/FormActionButton";
import Spinner from "../../Forms/Spinner/Spinner";
import Dialog from "@material-ui/core/Dialog/Dialog";
import mainStateHook from "../../../helpers/mainStateHook";
import {useMutation, useQuery} from "@apollo/react-hooks";
import {GET_ROLES, GET_USERS} from "../../../queries";
import {CREATE_USER, UPDATE_USER} from "../../../mutations";

const UserModal = ({open, handleClose, variables, selectedUser}) => {
  const [userForm, setUserForm] = mainStateHook({});
  const ingnoreFormFields = ['requestInProgress', 'valid', 'initiated'];
  const clearForm = () => {
    setUserForm({
      firstName: {
        valid: false,
        value: '',
      },
      lastName: {
        valid: false,
        value: ''
      },
      middleName: {
        valid: false,
        value: ''
      },
      email: {
        valid: false,
        value: ''
      },
      password: {
        valid: false,
        value: ''
      },
      roleId: {
        valid: true,
        value: ''
      },
      requestInProgress: false,
      valid: true,
      initiated: true
    });
    setAvatar({});
  };
  const validator = (value, fieldName) => {
    switch (fieldName) {
      case 'firstName':
      case 'lastName':
      case 'middleName':
        userForm[fieldName].valid = value ? /^[a-zA-Z]+$/.test(value) : false;
        break;
      case 'email':
        userForm.email.valid = value ? !!value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i) : false;
        break;
      case 'password':
        userForm.password.valid = value ?
          !!value.match(/^.*(?=.{8,})((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/): isEdit;
        break;
    }
    userForm[fieldName].value = value ? value : '';
    userForm.valid = Object.keys(userForm).filter(item => !ingnoreFormFields.includes(item))
      .reduce((c, key) => userForm[key].valid && c, true);
    setUserForm(userForm);
  };
  const [avatar, setAvatar] = useState({});
  let [isEdit, setIsEdit] = useState(false);

  useEffect(() => {
    isEdit = !!(selectedUser && selectedUser._id);
    setIsEdit(isEdit);
    clearForm();
    if (isEdit) {
      validator(selectedUser.firstName, 'firstName');
      validator(selectedUser.lastName, 'lastName');
      validator(selectedUser.middleName, 'middleName');
      validator(selectedUser.email, 'email');
      validator(selectedUser.role._id, 'roleId');
      validator('', 'password');
      setAvatar({
        fileContent: `data:image/png;base64,${selectedUser.avatar}`,
        fileExtension: 'png',
        isExternal: true
      });
    }
  }, [selectedUser]);

  useEffect(() => {
    clearForm();
  }, []);

  const [sendCreateForm] = useMutation(CREATE_USER);
  const [sendUpdateForm] = useMutation(UPDATE_USER);
  const {loading, data} = useQuery(GET_ROLES);

  const submitForm = () => {
    const request = {
      variables: {
        ...Object.keys(userForm)
          .filter(item => !ingnoreFormFields.includes(item))
          .reduce((c, key) => {
            c[key] = userForm[key].value;
            return c;
          }, {}),
        ...avatar.isExternal ? {} : {
          avatar: {
            fileContent: avatar.fileContent.includes('base64') ?
              avatar.fileContent.split(',')[1] :
              avatar.fileContent,
            fileExtension: avatar.fileExtension
          },
        },
        ...isEdit ? {_id: selectedUser._id} : {}
      },
      refetchQueries: [{
        query: GET_USERS,
        variables
      }]
    };
    const endAction = () => {
      handleClose();
      clearForm();
    };
    if(isEdit) {
      sendUpdateForm(request).then(endAction);
    } else {
      sendCreateForm(request).then(endAction);
    }
  };
  return <Dialog open={open} onClose={handleClose} fullWidth={true}>
    <DialogTitle>
      {isEdit ? 'Edit user' : 'Create user'}
    </DialogTitle>
    {userForm.initiated ? <div>
      <DialogContent>
        <div className={"flex"}>
          <Avatar avatarChanged={avatar => setAvatar(avatar)} initAvatar={avatar}/>
          <div className={"flex flex-grow flex-col items-center justify-between pt-5 pb-5"}>
            <FormInput width="3/4" type="text"
                       onChange={(e) => validator(e.target.value, 'firstName')}
                       placeholder="First Name"
                       margin={1}
                       value={userForm.firstName.value}
                       isValid={userForm.firstName.valid || !userForm.firstName.value}
            />
            <FormInput width="3/4" type="text"
                       onChange={(e) => validator(e.target.value, 'lastName')}
                       placeholder="Last Name"
                       value={userForm.lastName.value}
                       isValid={userForm.lastName.valid || !userForm.lastName.value}
                       margin={1}
            />
            <FormInput width="3/4" type="text"
                       onChange={(e) => validator(e.target.value, 'middleName')}
                       placeholder="Middle Name"
                       value={userForm.middleName.value}
                       isValid={userForm.middleName.valid || !userForm.middleName.value}
                       margin={1}
            />
            <FormInput width="3/4" type="email"
                       onChange={(e) => validator(e.target.value, 'email')}
                       placeholder="E-mail"
                       value={userForm.email.value}
                       isValid={userForm.email.valid || !userForm.email.value}
                       margin={1}
            />
            <FormInput width="3/4" type="password"
                                   onChange={(e) => validator(e.target.value, 'password')}
                                   placeholder="Password"
                                   value={userForm.password.value}
                                   isValid={userForm.password.valid || !userForm.password.value}
                                   margin={1}
            />
            <Dropdown items={loading ? [] : data.roles}
                      entityName='role'
                      value={userForm.roleId.value}
                      isValid='true'
                      onChange={(e) => validator(e.target.value, 'roleId')}
                      width="3/4"
            />
          </div>
        </div>
      </DialogContent>
      <DialogActions>
        <FormActionButton color="red" onClick={handleClose}>Cancel</FormActionButton>
        <FormActionButton disabled={!userForm.valid} onClick={e => submitForm(e)}>Save</FormActionButton>
      </DialogActions>
    </div> : <Spinner/>}

  </Dialog>
};

export default UserModal;