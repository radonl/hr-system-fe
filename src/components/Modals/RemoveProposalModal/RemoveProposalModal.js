import React from "react";
import {useMutation} from "@apollo/react-hooks";
import {REMOVE_PROPOSAL} from "../../../mutations";
import {GET_PROPOSALS} from "../../../queries";
import CommonRemoveModal from "../CommonRemoveModal/CommonRemoveModal";

const RemoveProposalModal = ({open, handleClose, selectedProposal, page, rowsPerPage}) => {
  const [removeProposal] = useMutation(REMOVE_PROPOSAL);
  const handleClickYes = () => {
    removeProposal({
      variables: {
        id: selectedProposal._id
      },
      refetchQueries: [{
        query: GET_PROPOSALS,
        variables: {
          skip: page * rowsPerPage,
          limit: rowsPerPage
        }
      }]
    }).then(() => handleClose()).catch(e => console.error(e));
  };
  const itemForRemove = selectedProposal && selectedProposal.candidate ?
    `${selectedProposal.candidate.firstName} ${selectedProposal.candidate.lastName} proposal` : '';
  return <CommonRemoveModal
    handleClickYes={() => handleClickYes()}
    handleClose={() => handleClose()}
    itemForRemove={itemForRemove}
    open={open}
  />
};

export default RemoveProposalModal;