import {Dialog} from "@material-ui/core";
import React from "react";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import FormActionButton from "../../Forms/Buttons/FormActionButton/FormActionButton";

const CommonRemoveModal = ({open, handleClose, handleClickYes, itemForRemove}) => {
  return <Dialog open={open} onClose={handleClose}>
      <DialogContent>
        Are you sure you want to
        delete {itemForRemove}?
      </DialogContent>
      <DialogActions>
        <FormActionButton color='red' onClick={() => handleClickYes()}>Yes</FormActionButton>
        <FormActionButton color='gray' onClick={() => handleClose()}>No</FormActionButton>
      </DialogActions>
    </Dialog>
};

export default CommonRemoveModal;