import React from "react";
import {useMutation} from "@apollo/react-hooks";
import {REMOVE_USER} from "../../../mutations";
import {GET_USERS} from "../../../queries";
import CommonRemoveModal from "../CommonRemoveModal/CommonRemoveModal";

const RemoveUserModal = ({open, handleClose, selectedUser, variables}) => {
  const [removeUser] = useMutation(REMOVE_USER);
  const handleClickYes = () => {
    removeUser({
      variables: {
        _id: selectedUser._id
      },
      refetchQueries: [{
        query: GET_USERS,
        variables
      }]
    }).then(() => handleClose()).catch(e => console.error(e));
  };
  const itemForRemove = selectedUser? `${selectedUser.firstName} ${selectedUser.lastName} user` : '';
  return <CommonRemoveModal
    handleClickYes={() => handleClickYes()}
    handleClose={() => handleClose()}
    itemForRemove={itemForRemove}
    open={open}
  />
};

export default RemoveUserModal;