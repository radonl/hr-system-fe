import {Dialog} from "@material-ui/core";
import DialogContent from "@material-ui/core/DialogContent";
import React, {useState} from "react";
import {useMutation, useQuery} from "@apollo/react-hooks";
import {GET_ME, GET_PROPOSALS} from "../../../queries";
import TextField from "@material-ui/core/TextField";
import SendIcon from '@material-ui/icons/Send';
import {CREATE_REVIEW} from "../../../mutations";

const ReviewModal = ({open, handleClose, selectedProposal, page, rowsPerPage}) => {
  const { loading, data } = useQuery(GET_ME);
  const [createReview] = useMutation(CREATE_REVIEW);
  const [message, setMessage] = useState('');
  const handleCreateReview = () => {
    createReview({
      variables: {
        _id: selectedProposal._id,
        message
      },
      refetchQueries: [{
        query: GET_PROPOSALS,
        variables: {
          skip: page * rowsPerPage,
          limit: rowsPerPage
        }
      }]
    }).then(() => {
      handleClose();
      setMessage('');
    }).catch(e => console.error(e));
  };
  return <Dialog open={open} onClose={handleClose}>
    <DialogContent>
      {selectedProposal.reviews && selectedProposal.reviews.map((review, index) => <div key={`${review._id}.${index}`} className={'flex bg-gray-300 m-1'}>
          <div className={'p-5 content-center items-center'}>
            <div className={`rounded-full w-32 h-32 bg-gray-500 bg-cover`}
                 style={{backgroundImage: review.reviewer.avatarFilePath ? `url(${review.reviewer.avatarFilePath})` : ''}}/>
            <div className={'text-center'}>
              <span>{review.reviewer.firstName} {review.reviewer.lastName}</span>
            </div>
          </div>
          <div className={'p-5 content-center items-center'}>
            <span>{review.message}</span>
          </div>
      </div>)}
      {!loading && <div className={'flex bg-gray-200'}>
        <div className={'p-5 content-center items-center'}>
          <div className={`rounded-full w-16 h-16 bg-gray-300 bg-cover`}
               style={{backgroundImage: data.me.avatar ? `url(data:image/png;base64,${data.me.avatar})` : ''}}/>
          <div className={'text-center'}>
            <span>{data.me.firstName} {data.me.lastName}</span>
          </div>
        </div>
        <div className={'p-5 content-center items-center w-full flex'}>
          <TextField multiline={true}
                     value={message}
                     size="medium"
                     label="Leave your review"
                     className="w-full"
                     onChange={e => setMessage(e.target.value)}
          />
          <SendIcon color="primary" className='cursor-pointer' onClick={() => handleCreateReview()}/>
        </div>
      </div>}
    </DialogContent>
  </Dialog>
};

export default ReviewModal;