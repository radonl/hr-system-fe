import DialogActions from "@material-ui/core/DialogActions";
import Dialog from "@material-ui/core/Dialog";
import React, {useEffect, useState} from "react";
import {useMutation, useQuery} from "@apollo/react-hooks";
import {DialogContent} from "@material-ui/core";
import moment from "moment";
import DialogTitle from "@material-ui/core/DialogTitle";

import FormActionButton from "../../Forms/Buttons/FormActionButton/FormActionButton";
import FormInput from "../../Forms/FormInput/FormInput";
import Avatar from "../../Forms/Avatar/Avatar";
import AttachFile from "../../Forms/AttachFile/AttachFile";
import mainStateHook from "../../../helpers/mainStateHook";
import MultiSelect from "../../Forms/DropDowns/MultiSelect/MultiSelect";
import Dropdown from "../../Forms/DropDowns/Dropdown/Dropdown";
import {GET_PROPOSALS, GET_SKILLS, GET_USERS} from "../../../queries";
import {CREATE_PROPOSAL, CREATE_SKILL, UPDATE_PROPOSAL} from "../../../mutations";
import {PROPOSAL_STATUSES} from "../../../helpers/constants";
import Spinner from "../../Forms/Spinner/Spinner";

const ProposalModal = ({open, handleClose, variables, selectedProposal}) => {
  const [proposalForm, setProposalForm] = mainStateHook({});
  const clearForm = () => {
    setProposalForm({
      firstName: {
        valid: false,
        value: '',
      },
      lastName: {
        valid: false,
        value: ''
      },
      middleName: {
        valid: false,
        value: ''
      },
      email: {
        valid: false,
        value: ''
      },
      phone: {
        valid: false,
        value: ''
      },
      desiredSalary: {
        valid: false,
        value: ''
      },
      suggestedSalary: {
        valid: false,
        value: ''
      },
      status: {
        valid: true,
        value: 'Waiting for an interview'
      },
      scheduledInterviewTime: {
        valid: false,
        value: moment().add(1, 'days').format('YYYY-MM-DDTHH:mm:ss')
      },
      requestInProgress: false,
      valid: true,
      initiated: true
    });
    setAvatar({});
    setCV({});
    setCandidateSkills([]);
    setSelectedTechEmployees([]);
  };
  const validator = (value, fieldName) => {
    switch (fieldName) {
      case 'firstName':
      case 'lastName':
      case 'middleName':
        proposalForm[fieldName].valid = value ? /^[a-zA-Z]+$/.test(value) : false;
        break;
      case 'email':
        proposalForm.email.valid = value ? !!value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i) : false;
        break;
      case 'phone':
        proposalForm.phone.valid = value ? !!value.match(/^[+]?[0-9]{10,12}/i) : false;
        break;
      case 'desiredSalary':
      case 'suggestedSalary':
        value = Number.parseInt(value, 10);
        proposalForm[fieldName].valid = value > 0;
        break;
      case 'scheduledInterviewTime':
        proposalForm.scheduledInterviewTime.valid = value ? moment(value).isAfter(moment()) : false;
        break;
      default:
        break;
    }
    proposalForm[fieldName].value = value ? value : '';
    proposalForm.valid = Object.keys(proposalForm).filter(item => item !== 'requestInProgress' && item !== 'valid' && item !== 'initiated')
      .reduce((c, key) => proposalForm[key].valid && c, true);
    setProposalForm(proposalForm);
  };
  const [candidateSkills, setCandidateSkills] = useState([]);
  const [selectedTechEmployees, setSelectedTechEmployees] = useState([]);
  const [avatar, setAvatar] = useState({});
  const [cv, setCV] = useState({});
  const [isEdit, setIsEdit] = useState(false);
  const [skills, techEmployees] = [
    useQuery(GET_SKILLS),
    useQuery(GET_USERS, {
      variables: {
        onlyTechEmployees: true,
        skip: 0,
        limit: 10000
      }
    })
  ];

  useEffect(() => {
    const isEdit = !!(selectedProposal && selectedProposal._id);
    setIsEdit(isEdit);
    clearForm();
    if (isEdit) {
      validator(selectedProposal.candidate.firstName, 'firstName');
      validator(selectedProposal.candidate.lastName, 'lastName');
      validator(selectedProposal.candidate.middleName, 'middleName');
      validator(selectedProposal.candidate.email, 'email');
      validator(selectedProposal.candidate.phone, 'phone');
      validator(selectedProposal.desiredSalary, 'desiredSalary');
      validator(selectedProposal.suggestedSalary, 'suggestedSalary');
      validator(moment(selectedProposal.scheduledInterviewTime).format('YYYY-MM-DDTHH:mm:ss'), 'scheduledInterviewTime');
      setCandidateSkills(skills.data.skills.filter(skill => !!selectedProposal.skills.find(item => item._id === skill._id)));
      setSelectedTechEmployees(techEmployees.data.users.filter(user => !!selectedProposal.techEmployees.find(item => item._id === user._id)));
      setAvatar({
        fileContent: selectedProposal.candidate.avatarFilePath,
        fileExtension: 'png',
        isExternal: true
      });
    }
  }, [selectedProposal]);

  useEffect(() => {
    clearForm();
  }, []);

  const [createSkill] = useMutation(CREATE_SKILL);
  const [sendCreateForm] = useMutation(CREATE_PROPOSAL);
  const [sendUpdateForm] = useMutation(UPDATE_PROPOSAL);
  const removeSkill = (e) => {
    setCandidateSkills(candidateSkills.filter(chip => chip._id !== e._id));
  };
  const removeTechEmployee = (e) => {
    setSelectedTechEmployees(selectedTechEmployees.filter(chip => chip._id !== e._id));
  };
  const addNewSkill = (name) => {
    if (!candidateSkills.find(skill => skill.name === name)) {
      const skill = skills.data.skills.find(skill => skill.name === name);
      if (!skill) {
        createSkill({variables: {name}}).then(({data}) => {
          candidateSkills.push(data.createSkill);
          setCandidateSkills(candidateSkills);
        })
      } else {
        candidateSkills.push(skill);
        setCandidateSkills(candidateSkills);
      }
    }
  };
  const addNewTechEmployee = (name) => {
    const user = techEmployees.data.users.find(user => user.name === name);
    if (user && !selectedTechEmployees.find(employee => employee.name === user.name)) {
      selectedTechEmployees.push(user);
      setSelectedTechEmployees(selectedTechEmployees);
    }
  };
  const submitForm = () => {
    const request = {
      variables: {
        ...Object.keys(proposalForm)
          .filter(item => item !== 'requestInProgress' && item !== 'valid' && item !== 'initiated')
          .reduce((c, key) => {
            c[key] = proposalForm[key].value;
            return c;
          }, {}),
        cv,
        ...avatar.isExternal ? {} : {
          avatar: {
            fileContent: avatar.fileContent.includes('base64') ?
              avatar.fileContent.split(',')[1] :
              avatar.fileContent,
            fileExtension: avatar.fileExtension
          },
        },
        skills: candidateSkills.map(skill => skill._id),
        techEmployees: selectedTechEmployees.map(user => user._id),
        ...isEdit ? {_id: selectedProposal._id} : {}
      },
      refetchQueries: [{
        query: GET_PROPOSALS,
        variables
      }]
    };
    const endAction = () => {
      handleClose();
      clearForm();
    };
    if(isEdit) {
      sendUpdateForm(request).then(endAction);
    } else {
      sendCreateForm(request).then(endAction);
    }
  };
  return <Dialog open={open} onClose={handleClose} fullWidth={true}>
    <DialogTitle>
      {isEdit ? 'Edit proposal' : 'Create proposal'}
    </DialogTitle>
    {proposalForm.initiated ? <div>
      <DialogContent>
        <div className={"flex"}>
          <Avatar avatarChanged={avatar => setAvatar(avatar)} initAvatar={avatar}/>
          <div className={"flex flex-grow flex-col items-center justify-between pt-5 pb-5"}>
            <FormInput width="3/4" type="text"
                       onChange={(e) => validator(e.target.value, 'firstName')}
                       placeholder="First Name"
                       margin={1}
                       value={proposalForm.firstName.value}
                       isValid={proposalForm.firstName.valid || !proposalForm.firstName.value}
            />
            <FormInput width="3/4" type="text"
                       onChange={(e) => validator(e.target.value, 'lastName')}
                       placeholder="Last Name"
                       value={proposalForm.lastName.value}
                       isValid={proposalForm.lastName.valid || !proposalForm.lastName.value}
                       margin={1}
            />
            <FormInput width="3/4" type="text"
                       onChange={(e) => validator(e.target.value, 'middleName')}
                       placeholder="Middle Name"
                       value={proposalForm.middleName.value}
                       isValid={proposalForm.middleName.valid || !proposalForm.middleName.value}
                       margin={1}
            />
            <FormInput width="3/4" type="email"
                       onChange={(e) => validator(e.target.value, 'email')}
                       placeholder="E-mail"
                       value={proposalForm.email.value}
                       isValid={proposalForm.email.valid || !proposalForm.email.value}
                       margin={1}
            />
            <FormInput width="3/4" type="text"
                       onChange={(e) => validator(e.target.value, 'phone')}
                       placeholder="Phone"
                       value={proposalForm.phone.value}
                       isValid={proposalForm.phone.valid || !proposalForm.phone.value}
                       margin={1}
            />
          </div>
        </div>
        <MultiSelect addHandler={value => addNewSkill(value)}
                     items={skills.loading ? [] : skills.data.skills}
                     selectedItems={candidateSkills}
                     label="Add new skill"
                     loading={skills.loading}
                     removeHandler={item => removeSkill(item)}
        />
        <MultiSelect addHandler={value => addNewTechEmployee(value)}
                     items={techEmployees.loading ? [] : techEmployees.data.users}
                     selectedItems={selectedTechEmployees}
                     label="Add new tech employee"
                     loading={techEmployees.loading}
                     removeHandler={item => removeTechEmployee(item)}
        />
        <div className={"flex"}>
          <div className={"flex flex-grow flex-col items-center justify-between"}>
            <FormInput width="3/4"
                       isValid="true"
                       type="number"
                       onChange={(e) => validator(e.target.value, 'desiredSalary')}
                       value={proposalForm.desiredSalary.value}
                       placeholder="Desired Salary"
                       margin={1}
            />
            <FormInput width="3/4"
                       isValid="true"
                       type="number"
                       onChange={(e) => validator(e.target.value, 'suggestedSalary')}
                       value={proposalForm.suggestedSalary.value}
                       placeholder="Suggested Salary"
                       margin={1}
            />
            <FormInput width="3/4"
                       isValid="true"
                       type="datetime-local"
                       defaultValue={proposalForm.scheduledInterviewTime.value}
                       onChange={(e) => validator(e.target.value, 'scheduledInterviewTime')}
                       placeholder="Scheduled Interview Time"
                       margin={1}
            />
            <Dropdown items={PROPOSAL_STATUSES}
                      entityName='status'
                      isValid='true'
                      onChange={(e) => validator(e.target.value, 'status')}
                      width="3/4"
            />
          </div>
          <div className={"flex items-center p-8"}>
            <AttachFile fileChanged={file => setCV(file)} label="Upload CV" initFile={selectedProposal.cv ? selectedProposal.cv : null}/>
          </div>
        </div>
      </DialogContent>
      <DialogActions>
        <FormActionButton color="red" onClick={handleClose}>Cancel</FormActionButton>
        <FormActionButton disabled={!proposalForm.valid} onClick={e => submitForm(e)}>Save</FormActionButton>
      </DialogActions>
    </div> : <Spinner/>}

  </Dialog>
};

export default ProposalModal;
